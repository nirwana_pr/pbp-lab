from django import forms
from .models import Friend

class FriendForm(forms.ModelForm):
	class Meta:
		model = Friend
		fields = '__all__'
	error_messages = {
		'required' : 'Please Type'
	}

	