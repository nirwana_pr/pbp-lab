from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from .models import Friend
from .forms import FriendForm

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values() 
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm()

    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')

    response = {'form' : form} 
    return render(request, 'lab3_form.html',response)