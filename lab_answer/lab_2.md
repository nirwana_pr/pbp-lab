1)  Perbedaan JSON dan XML
    - JSON (JavaScript Object Notation) dikembangkan dari Javascript, sementara XML (Extensible markup language) dikembangkan dari SGML(Standard Generalized Markup Language)
    - JSON tidak menggunakan tag structure untuk merepresentasikan datanya, sementara XML menggunakan tag pada representasi datanya
    - JSON tidak mensupport namespaces, sementara XML mensupport namespaces
    - Dokumen JSON lebih mudah dibaca dibandingkan dengan XML karena strukturnya yang tanpa tag terlihat lebih sederhana
    - JSON mendukung penggunaan array, sementara XML tidak
    - JSON hanya mendukung UTF-8 encoding, sementara XML cakupan encodingnya lebih luas (tidak terbatas pada UTF-8)
    - Dari segi keamanan, JSON cenderung lebih tidak aman jika dibandingkan dengan XML
    - JSON menggunakan struktur data map, sedangkan XML menggunakan struktur data tree


2)  Perbedaan XML dan HTML
    - Kepanjangan dari XML adalah eXtensible Markup Language, sedangkan HTML adalah Hyper Text Markup Language
    - XML lebih terfokus pada fungsi mentransfer data, sementara HTML fokusnya lebih kepada bagaimana data tersebut ditampilkan pada user
    - XML content driven, sementara HTML format driven
    - XML bisa menggunakan custom tags, sementara tags pada HTML sudah ditentukan sebelumnya (pre-defined)
    - XML case sensitive, sementara HTML tidak
    - Tags pada XML harus ada opening dan closing-nya, sementara HTML ada yang tidak mengapa tanpa closing tags, seperti tag <br>
    - XML mensupport namespaces, sedangkan HTML tidak